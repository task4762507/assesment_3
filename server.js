require("dotenv").config();
const express = require("express");
const app = express();
const { Server } = require("socket.io");
const { createServer } = require("http");
const cors = require("cors");
const server = createServer(app);
const connection = require("./common/connection");
const socket = require("./v1/sockets/index");
const MainRouter = require("./v1/Routes/index");
const swaggerUi = require("swagger-ui-express");
const YAML = require("yamljs");
const services = require("./services/swagger");
const swaggerDocument = YAML.load("./swagger/collection.yml");
const io = new Server(server, {
  cors: {
    origin: "*",
    methods: ["GET", "POST"],
    credentials: true,
  },
});

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use(express.json());
app.use(cors());
app.use("/api/v1", MainRouter);

server.listen(process.env.PORT, () => {
  connection(), socket(io), services.createSwagger();
});
