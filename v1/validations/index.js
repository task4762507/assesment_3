const joi=require('joi')

const userValidation=joi.object({
    username:joi.string().required(),
    email:joi.string().email(),
    password:joi.string().required().min(8).max(16)
})
const loginValidations=joi.object({
    email:joi.string().required(),
    password:joi.string().required().min(8).max(16)
})
module.exports={
    userValidation,
    loginValidations
}