const mongoose=require('mongoose')

const ChatSchema=new mongoose.Schema({

    RoomID:{
        type:String,
        ref:'room'
    },
   
    message:{
        type:String
    }

},
{
    timestamps:true
}
)

module.exports=ChatSchema