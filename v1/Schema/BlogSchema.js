const mongoose=require('mongoose')

const BlogSchema=new mongoose.Schema({
    Blogid:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'profile'
    },
    postImage:{
        type:String
    },
    caption:{
        type:String,
        maxLength:100
    }
})

module.exports=BlogSchema