const mongoose=require('mongoose')

const UserSchema=new mongoose.Schema({
    Username:{
        type:String,
        lowercase:true
    },
    email:{
        type:String,
        lowercase:true
    },
    password:{
        type:String,
        
    },
    jti:{
        type:String
    }
},
{
    timestamps:true
}
)

module.exports=UserSchema