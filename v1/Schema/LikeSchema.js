const mongoose=require('mongoose')

const LikeSchema=new mongoose.Schema({
    BlogId:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'blog'
    },
    UserID:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'user'
    },
    like:{
        type:Number
    }
},{
    timestamps:true
})

module.exports=LikeSchema