const mongoose=require('mongoose')

const CommentSchema=new mongoose.Schema({
    BlogId:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'blog'
    },
    UserID:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'user'
    },
    comment:{
        type:String,
        maxLength:50
    }
},
{
    timestamps:true
}
)

module.exports=CommentSchema