const mongoose=require('mongoose')

const ProfileSchema=new mongoose.Schema({
    Userid:{
    type:mongoose.Schema.Types.ObjectId,
    ref:'user'
    },
    image:{
        type:String
    },
    bio:{
        type:String
    }
})

module.exports=ProfileSchema