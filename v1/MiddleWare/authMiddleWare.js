const jwt = require("jsonwebtoken");
const DB = require("../Models");


// This function will take care of the verification for the user JWT token and JTI
async function authMiddleware(req, res, next) {
  const jwtToken = req.headers.authorization;

  const token = jwtToken.split(" ")[1];

  const decode = jwt.verify(token, process.env.JWT_SECRET);
  if (decode) {
    req.userId = decode.userID;
    const jwtId = decode.jti;

    const con = await DB.user.findOne({ 
      $and:[
        {jti:jwtId},
        {_id:decode.userID}
      ]
      

    });

    if (con) {
      next();
    } else {
      return res.json({
        msg: "session expired",
      });
    }
  } else {
    res.json({
      msg: "invalid token",
    });
  }
}

module.exports=authMiddleware
