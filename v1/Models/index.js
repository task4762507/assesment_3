const mongoose = require("mongoose");
const UserSchema = require("../Schema/UserSchema");
const ProfileSchema = require("../Schema/ProfileSchema");
const BlogSchema = require("../Schema/BlogSchema");
const LikeSchema = require("../Schema/LikeSchema");
const CommentSchema = require("../Schema/CommentSchema");
const RoomSchema = require("../Schema/RoomSchema");
const ChatSchema = require("../Schema/ChatSchema");

// mongo_db models are created and exported
const user = new mongoose.model("user", UserSchema);
const profile = new mongoose.model("profile", ProfileSchema);
const blog = new mongoose.model("blog", BlogSchema);
const like=new mongoose.model('like',LikeSchema)
const comment=new mongoose.model('comment',CommentSchema)
const room=new mongoose.model('room',RoomSchema)
const chat=new mongoose.model('chat',ChatSchema)

module.exports = {
  user,
  profile,
  blog,
  like,
  comment,
  room,
  chat
};
