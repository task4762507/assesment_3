const DB = require("../../Models");

module.exports.postBlog = async (req, res) => {
  try {
    const id = req.query.id;
    const image = req.body.image;
    const caption = req.body.caption;
    const checkProfile = await DB.profile.findOne({ _id: id });
    if (!checkProfile) {
      return res.status(400).json({
        msg: "First create your Profile",
      });
    }
    const blog = await DB.blog.create({
      Blogid: id,
      postImage: image,
      caption: caption,
    });

    return res.status(200).json({
      msg: "Blog Published",
      blog,
    });
  } catch (error) {
    return res.status(400).json({
      msg: "try again",
    });
  }
};

module.exports.updateBlog = async (req, res) => {
  try {
    const id = req.query.id;
    const caption = req.body.caption;
    const checkBlog = await DB.blog.findOne({ _id: id });
    if (!checkBlog) {
      return res.status(400).json({
        msg: "blog does not exist",
      });
    }
    const filter = { _id: id };

    const updatedBody = {
      caption: caption,
    };
    const updatedBlog = await DB.blog.findOneAndUpdate(filter, updatedBody);

    return res.status(200).json({
      msg: "Updated blog",
      updatedBlog,
    });
  } catch {
    return res.status(400).json({
      msg: "sorry for the trouble",
    });
  }
};

module.exports.deleteBlog = async (req, res) => {
  try {
    const id = req.query.id;
    const checkBlog = await DB.blog.findOne({ _id: id });
    if (!checkBlog) {
      return res.status(400).json({
        msg: "blog does not exist",
      });
    }
    await DB.blog.deleteOne({ _id: id });
    return res.status(200).json({
      msg: "user deleted",
    });
  } catch (error) {
    return res.status(400).json({
      msg: error,
    });
  }
};
