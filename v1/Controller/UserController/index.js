const DB = require("../../Models");
const agenda = require("../../Nodemailer");
const Validations = require("../../validations/index");
const bcrypt = require("bcrypt");
const mongodb = require("mongodb");
const jwt = require("jsonwebtoken");
const { v4: uuidv4 } = require("uuid");
const saltRounds = 10;

module.exports.getAll = async (req, res) => {
  const page = req.query.page || 1;
  const limit = req.query.limit;
  const skip = (page - 1) * limit;
  const filter = req.query.filter;
  if (filter == undefined) {
    const users = await DB.user
      .find({})
      .skip(skip)
      .limit(limit)
      .select({ jti: 0, password: 0 });
    const length = await DB.user.countDocuments({});
    return res.json({
      users,
      length,
    });
  } else {
    let result = filter.trim();
    const length = await DB.user.countDocuments({
      Username: {
        "$regex": result,
      },
    });
    // console.log(result)
    const users = await DB.user
      .find({
        Username: {
          "$regex": result,
        },
      })
      .skip(skip)
      .limit(limit)
      .select({ jti: 0, password: 0 });

    return res.json({
      users,
      length,
    });
  }
};

module.exports.getProfile = async (req, res) => {
  const id = req.query.id;

 
  const objectId = new mongodb.ObjectId(id);

  // applying aggregation to get user profile
  const userProfile = await DB.user.aggregate([
    {
      $match: {
        _id: objectId,
      },
    },
    {
      $lookup: {
        from: "profiles",
        localField: "_id",
        foreignField: "Userid",
        as: "Output",
      },
    },
    {
      $unwind: "$Output",
    },
    {
      $project: {
        Username: 1,
        email: 1,
        image: "$Output.image",
        bio: "$Output.bio",
      },
    },
  ]);
  console.log(userProfile)
  if(userProfile.length<1){
    return res.status(400).json({
      msg:"user not found"
    })
  }

  return res.status(200).json({
    userProfile,
  });
};
module.exports.postUser = async (req, res) => {
  try {
    const validateUser = await Validations.userValidation.validateAsync(
      req.body
    );
    const username = req.body.username;
    const email = req.body.email;
    const password = req.body.password;
    const exist = await DB.user.findOne({ email: email });
    if (exist) {
      return res.status(400).json({
        msg: "User Already Exist Please Login",
      });
    }

    const hashPassword = await bcrypt.hash(password, saltRounds);
    const created = await DB.user.create({
      Username: username,
      email: email,
      password: hashPassword,
    });
    await agenda.schedule("in 1 minute", "sendMail", {
      to: created.email,
      username: created.username,
    });
    return res.status(200).json({
      msg: "User Created",
    });
  } catch (error) {
    return res.status(400).json({
      error,
    });
  }
};
module.exports.login = async (req, res) => {
  try {
    console.log(process.env.JWT_SECRET);
    const validateUser = await Validations.loginValidations.validateAsync(
      req.body
    );

    const email = req.body.email;
    const password = req.body.password;

    const exist = await DB.user.findOne({ email: email });

    if (!exist) {
      return res.status(400).json({
        msg: "First Create Your Account",
      });
    }

    const pw = exist.password;
    const hash = await bcrypt.compare(password, pw);
    if (hash) {
      const userID = exist._id;
      const jti = uuidv4();
      const data = await DB.user.updateOne({ _id: userID, jti: jti });
      const token = jwt.sign(
        { userID: userID, jti, role: "user" },
        process.env.JWT_SECRET
      );
      return res.status(200).json({
        msg: "logged in",
        token,
      });
    } else {
      return res.status(400).json({
        msg: "incorrect password",
      });
    }
  } catch (error) {
    res.status(400).json({
      msg: "error",
      error,
    });
  }
};

module.exports.createProfile = async (req, res) => {
  try {
    const id = req.query.id;
    const image = req.body.image;
    const bio = req.body.bio;

    await DB.profile.create({
      Userid: id,
      image: image,
      bio: bio,
    });
    return res.status(200).json({
      msg: "thank you for creating your profile with us",
    });
  } catch (error) {
    return res.status(400).json({
      msg: "unable to create profile",
    });
  }
};

module.exports.updateProfile = async (req, res) => {
  try {
    const id = req.query.id;

    const exist = await DB.profile.findOne({ Userid: id });
    console.log(exist);
    if (!exist) {
      return res.status(400).json({
        msg: "First create your profile",
      });
    }
    const filter = { Userid: id };

    console.log(filter);
    const updatedBody = {
      image: req.body.image,
      bio: req.body.bio,
    };
    const updatedUser = await DB.profile.findOneAndUpdate(filter, updatedBody);
    console.log(updatedBody);
    return res.status(200).json({
      msg: "profile updated",
    });
  } catch (error) {
    return res.status(400).json({
      msg: "sorry for the interruption",
    });
  }
};

module.exports.deleteProfile = async (req, res) => {
  try {
    const id = req.query.id;
    const exist = await DB.profile.findOne({ Userid: id });

    if (!exist) {
      return res.status(400).json({
        msg: "User not Found",
      });
    }
    await DB.profile.deleteOne({ useriD: id });
    return res.status(200).json({
      msg: "user deleted",
    });
  } catch (error) {
    return res.status(400).json({
      msg: "User does not exist",
    });
  }
};
