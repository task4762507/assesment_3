const { google }=require('googleapis')
const nodemailer=require('nodemailer')

const Agenda=require('agenda')

const agenda=new Agenda({db:{address:process.env.MONGO_DB}})

const CLIENT_ID =process.env.CLIENT_ID;
const CLIENT_SECRET = process.env.CLIENT_SECRET;
const REFRESH_TOKEN = process.env.REFRESH_TOKEN;
const REDIRECT_URI = process.env.REDIRECT_URI; 
const MY_EMAIL =process.env.MY_EMAIL;


const oAuth2Client = new google.auth.OAuth2(
  CLIENT_ID,
  CLIENT_SECRET,
  REDIRECT_URI
);

oAuth2Client.setCredentials({ refresh_token: REFRESH_TOKEN });

agenda.define('sendMail',async(jobs)=>{
    const {to,username}=jobs.attr.data
    const ACCESS_TOKEN = await oAuth2Client.getAccessToken();
    const transport = nodemailer.createTransport({
      service: "gmail",
      auth: {
        type: "OAuth2",
        user: MY_EMAIL,
        clientId: CLIENT_ID,
        clientSecret: CLIENT_SECRET,
        refreshToken: REFRESH_TOKEN,
        accessToken: ACCESS_TOKEN,
      },
      tls: {
        rejectUnauthorized: true,
      },
    });

    const from = MY_EMAIL;
    const subject = " Greetings";
    const replyTo='fullstacktester@yopmail.com'
  
    const html = `
      <p>Hey ${to},</p>
      <p>${username}</p>
      <p>your database is created</p>
      `;
      await transport.sendMail({ from, subject, to, html,replyTo })
      return;
})

const start= async()=>{
    await agenda.start()
 }
// agenda.schedule()
start()

module.exports=agenda