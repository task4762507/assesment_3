const mongoose = require("mongoose");
const DB = require("../Models/index");

module.exports = (io) => {
  io.on("connection", (socket) => {
    // user will join its room when online
    socket.on("onlineUser", ({ userId }) => {
      console.log("user joined");
      socket.join(userId);
    });

    // api for handling the like 
    socket.on("like-blog", async ({ blogId, userId }) => {
      try {
        const exist = await DB.like.findOne({ UserID: userId });
        if (exist) {
          // disliking the blog by deleting the like
          const deleted = await DB.like.deleteOne({ UserID: userId });
          //returning the new count of likes
          const totalCount = await DB.like.countDocuments({ BlogId: blogId });
          return io.emit("liked", totalCount);
        }

        const liked = await DB.like.create({
          BlogId: blogId,
          UserID: userId,
          like: 1,
        });
        // counting the number of likes and returning
        const totalCount = await DB.like.countDocuments({ BlogId: blogId });

        return io.emit("liked", totalCount);
      } catch (error) {
        socket.to(userId).emit("like-response", "unable to like");
      }
    });

    //this api will handle the commenting on the post
    socket.on("comment-blog", async ({ blogId, userId, comment }) => {
      // commenting on the blogs as one user can have multiple comments 
      try {
        await DB.comment.create({
          BlogId: blogId,
          UserID: userId,
          comment: comment,
        });

        const totalComments = await DB.comment
          .find({})
          .select({ comment: 1, UserID: 1, _id: 0 });

        return io.emit("total-comment", totalComments);
      } catch (error) {
        return io.emit("total-comments", "unable to delete");
      }
    });


    // this api will handle the delete operations on comment
    socket.on("delete-comment", async ({ blogId, userId, comment }) => {
      try {

        const exist=await DB.comment.findOne({
          $and:[
            {BlogId:blogId},
            {UserID:userId},
            {comment:comment}
          ]
        })
        if(exist){
          await DB.comment.deleteOne({
            $and:[
              {BlogId:blogId},
              {UserID:userId},
              {comment:comment}
            ]
          })
          const totalComment=await DB.comment.find({}).select({comment: 1, UserID: 1, _id: 0})
          return io.emit("deleted", totalComment);
        }
       

      } catch (error) {
        return io.emit("deleted", "unable to delete");
      }
    });

    socket.on('create-room',async({userOne,userTwo,})=>{
         try{
          const exist =await DB.room.findOne({
            $and:[
              {UserOne:userOne},
              {UserTwo:userTwo},
              
    
            ]})
          console.log(exist)
          const roomid=Math.floor((Math.random()*10)+1)
          if(!exist){
          console.log("g")
          console.log(userOne)
           const Room= await DB.room.create({
              RoomId:roomid,
              UserOne:userOne,
              UserTwo:userTwo
            })

            return socket.join(roomid)
          }else{
            socket.join(roomid)
          }
         }catch(error){
          return io.emit('create-room',"unable to create the room")
         }
 })

 socket.on('chat',async({msg,userId,userTwo})=>{
   try{
    console.log("gg")
    console.log(userId)
   
    const exist =await DB.room.findOne({
      $and:[
        {UserOne:userId},
        {UserTwo:userTwo},
        

      ]})
      console.log(exist)
      if(exist){
        await DB.chat.create({
          RoomID:exist.RoomId,
          message:msg,
          
        })
       return  io.to(exist.RoomId).emit('receive',msg)
      }
     
      

   }catch{
      return io.emit('receive',"jj")
   }
 })





  });
};
