const express = require("express");
const router = express.Router();
const UserRouter = require("./UserRoutes");
const BlogRoutes = require("./BlogRoutes");

router.use("/user", UserRouter);
router.use("/blogs", BlogRoutes);

module.exports = router;
