const express = require("express");
const router = express.Router();
const UserController = require("../Controller/UserController/index");
const authMiddleware = require("../MiddleWare/authMiddleWare");

// this route will get the all users the are present in the database
router.get("/getAll", UserController.getAll);

router.get("/getUser", UserController.getProfile);
// Creating a user for the first time
router.post("/postUser", UserController.postUser);

//login Route and jwt token will be generated
router.post("/login", UserController.login);

//creating user profile with authentication
router.post("/createProfile", authMiddleware, UserController.createProfile);

// in this user will be able to update the profile for picture and bio
router.post("/updateProfile", authMiddleware, UserController.updateProfile);

// for deleting the user profile
router.post("/deleteProfile", authMiddleware, UserController.deleteProfile);

module.exports = router;
