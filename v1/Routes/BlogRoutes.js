const express = require("express");
const BlogController=require('../Controller/BlogController/index')
const authMiddleware = require("../MiddleWare/authMiddleWare");
const router = express.Router();

// this will create a blog post for the user profile

router.post("/blogPost", authMiddleware,BlogController.postBlog);

//this will update the blog of the user 
router.post("/updateBlog", authMiddleware,BlogController.updateBlog);

//this will delete blog

router.post("/deleteBlog", authMiddleware,BlogController.updateBlog);

module.exports = router;
